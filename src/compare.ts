/**
 * returns array with common values
 * @param unique ? if true, removes duplicates
 */
export function arrayIntersection(
  arr1: unknown[],
  arr2: unknown[],
  unique?: boolean
): unknown[] {
  const diff = arr1.filter((item) => arr2.includes(item));
  if (unique) {
    return [...new Set(diff)];
  }
  return diff;
}

/**
 * returns array with different values in first array
 */
export function arrayDifference(arr1: unknown[], arr2: unknown[]): unknown[] {
  return arr1.filter((item) => !arr2.includes(item));
}

/**
 * returns array with different values in both arrays
 */
export function arraySymmetricDifference(
  arr1: unknown[],
  arr2: unknown[]
): unknown[] {
  return arr1
    .filter((item) => !arr2.includes(item))
    .concat(arr2.filter((item) => !arr1.includes(item)));
}
