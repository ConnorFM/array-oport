import { floatToInteger } from './split';

export function findIndex(
  array: unknown[],
  predicate: (arrayIndex: unknown, index: number, array: unknown[]) => boolean,
  fromIndex?: number,
  fromRight?: boolean
): number {
  if (!fromIndex) {
    fromIndex = 0;
  }
  const { length } = array;
  let index = fromIndex + (fromRight ? 1 : -1);
  while (fromRight ? index-- : ++index < length) {
    if (predicate(array[index], index, array)) {
      return index;
    }
  }
  return -1;
}

export function findLastIndex(
  array: unknown[],
  predicate: (arrayIndex: unknown, index: number, array: unknown[]) => boolean,
  fromIndex?: number
): number {
  const length = array == null ? 0 : array.length;
  if (!length) {
    return -1;
  }
  let index = length - 1;
  if (fromIndex !== undefined) {
    index = floatToInteger(fromIndex);
    index =
      fromIndex < 0 ? Math.max(length + index, 0) : Math.min(index, length - 1);
  }
  return findIndex(array, predicate, index, true);
}

export function findFirst(array: unknown[]): unknown {
  return array && array.length ? array[0] : undefined;
}

export function findLast(array: unknown[]): unknown {
  const length = array == null ? 0 : array.length;
  return length ? array[length - 1] : undefined;
}

export function findAny(array: unknown[], index: number): unknown {
  const length = array == null ? 0 : array.length;
  if (!length) {
    return;
  }
  index += index < 0 ? length : 0;
  return array[index];
}

export function findRandom(array: unknown[]): unknown {
  const length = array == null ? 0 : array.length;
  return length ? array[Math.floor(Math.random() * length)] : undefined;
}
