export function arrayBurst(array: unknown[], size = 1): unknown[] {
  size = Math.max(floatToInteger(size), 0);
  const length = array == null ? 0 : array.length;
  if (!length || size < 1) {
    return [];
  }
  let index = 0;
  let resIndex = 0;
  const result = new Array(Math.ceil(length / size));

  while (index < length) {
    result[resIndex++] = array.slice(index, (index += size));
  }
  return result;
}

export function floatToInteger(number: number): number {
  const ceil = Math.ceil(number);
  const floor = Math.floor(number);
  if (ceil - number >= 0.5) {
    return floor;
  } else {
    return ceil;
  }
}
