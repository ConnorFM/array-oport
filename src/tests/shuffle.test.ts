import { arrayShuffle } from '../shuffle';

describe('arrayShuffle', () => {
  test('result array to be different from original', () => {
    expect(arrayShuffle([0, 1, false, 2, '', 3, 'soleil'])).not.toEqual([
      [0],
      [1],
      [false],
      [2],
      [''],
      [3],
      ['soleil'],
    ]);
    expect(
      arrayShuffle([0, 1, false, 2, '', 3, 'soleil']).length
    ).toStrictEqual([0, 1, false, 2, '', 3, 'soleil'].length);
    expect(arrayShuffle([0, 1, false, 2, '', 3, 'soleil']).sort).toStrictEqual(
      [0, 1, false, 2, '', 3, 'soleil'].sort
    );
  });
});
