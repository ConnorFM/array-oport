import {
  findAny,
  findFirst,
  findIndex,
  findLast,
  findLastIndex,
  findRandom,
} from '../find';
const users = [
  { user: 'merry', active: false },
  { user: 'frodo', active: true },
  { user: 'sam', active: true },
  { user: 'pippin', active: false },
];
describe('findIndex', () => {
  test('function to find index of user existing', () => {
    expect(findIndex(users, ({ user }) => user === 'sam')).toStrictEqual(2);
  });
  test('function to find index of user not existing', () => {
    expect(findIndex(users, ({ user }) => user == 'aragorn')).toStrictEqual(-1);
  });
  test('function to find index of second user non active', () => {
    expect(findIndex(users, ({ active }) => active == false, 1)).toStrictEqual(
      3
    );
  });
});

describe('findLastIndex', () => {
  test('function to find index of last user active', () => {
    expect(
      findLastIndex(users, ({ active }) => active === false)
    ).toStrictEqual(3);
  });
  test('function to find index of user not existing', () => {
    expect(findLastIndex(users, ({ user }) => user == 'aragorn')).toStrictEqual(
      -1
    );
  });
  test('function to find index of second user non active', () => {
    expect(
      findLastIndex(users, ({ active }) => active == false, 1)
    ).toStrictEqual(0);
  });
  test('function to find null array', () => {
    expect(
      findLastIndex(null, ({ active }) => active == false, 1)
    ).toStrictEqual(-1);
  });
  test('function to find index of user non active, start negative', () => {
    expect(
      findLastIndex(users, ({ active }) => active == false, -1)
    ).toStrictEqual(3);
  });
});

describe('findFirst', () => {
  test('function to get first value', () => {
    expect(findFirst(users)).toStrictEqual({ user: 'merry', active: false });
  });
  test('function to get no value', () => {
    expect(findFirst([])).toBeUndefined();
  });
});

describe('findLast', () => {
  test('function to get last value', () => {
    expect(findLast(users)).toStrictEqual({ user: 'pippin', active: false });
  });
  test('function to get no value', () => {
    expect(findLast(null)).toBeUndefined();
  });
});

describe('findAny', () => {
  test('function to get third value', () => {
    expect(findAny(users, 2)).toStrictEqual({ user: 'sam', active: true });
  });
  test('function to get the value before the last', () => {
    expect(findAny(users, -2)).toStrictEqual({ user: 'sam', active: true });
  });
  test('function to get no value', () => {
    expect(findAny(null, 2)).toBeUndefined();
  });
});

describe('findRandom', () => {
  test('value to be in array', () => {
    expect(users).toContain(findRandom(users));
  });
  test('value got to be the expected type', () => {
    // eslint-disable-next-line prettier/prettier
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    expect(typeof (findRandom(users) as any).user).toEqual('string');
  });
  test('function to get no value', () => {
    expect(findRandom(null)).toBeUndefined();
  });
});
