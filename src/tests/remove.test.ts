import {
  removeOnce,
  removeAll,
  removeMultiValues,
  removeDuplicates,
  removeFalsy,
  arrayLongSlice,
  arrayLongSliceEnd,
} from '../remove';

const arrayNumber = [2, 5, 9, 1, 5, 8, 5];
const arrayString = ['2', '5', '9', '1', '5', '8', '5'];
const arrayMixed = ['2', '5', '9', '1', 5, '8', 5];

describe('removeOnce', () => {
  test('function to remove value once', () => {
    expect(removeOnce(arrayNumber, 5)).toStrictEqual([2, 9, 1, 5, 8, 5]);
  });
  test('function to remove value once with array of strings', () => {
    expect(removeOnce(arrayString, '5')).toStrictEqual([
      '2',
      '9',
      '1',
      '5',
      '8',
      '5',
    ]);
  });
  test('function to remove value once with a mixed array', () => {
    expect(removeOnce(arrayMixed, 5)).toStrictEqual([
      '2',
      '5',
      '9',
      '1',
      '8',
      5,
    ]);
    expect(removeOnce(arrayMixed, '5')).toStrictEqual([
      '2',
      '9',
      '1',
      5,
      '8',
      5,
    ]);
  });
  test('function to not remove any value', () => {
    expect(removeOnce(arrayNumber, 6)).toStrictEqual(arrayNumber);
  });
});

describe('removeAll', () => {
  test('function to remove all values', () => {
    expect(removeAll(arrayNumber, 5)).toStrictEqual([2, 9, 1, 8]);
  });
  test('function to not remove any value', () => {
    expect(removeAll(arrayNumber, 6)).toStrictEqual(arrayNumber);
  });
});

describe('removeMultipleValue', () => {
  test('function to remove all values', () => {
    expect(removeMultiValues(arrayNumber, [1, 2, 5])).toStrictEqual([9, 8]);
  });
  test('function to not remove any value', () => {
    expect(removeMultiValues(arrayNumber, [])).toStrictEqual(arrayNumber);
  });
});

describe('removeDuplicates', () => {
  test('function to remove all duplicates', () => {
    expect(removeDuplicates(arrayString)).toStrictEqual([
      '2',
      '5',
      '9',
      '1',
      '8',
    ]);
  });
});

describe('removeFalsey', () => {
  test('function to remove all falsey values', () => {
    expect(removeFalsy([0, 1, false, 2, '', 3, 'soleil'])).toStrictEqual([
      1,
      2,
      3,
      'soleil',
    ]);
  });
});

describe('arrayLongSlice', () => {
  test('function to remove first 4 values', () => {
    expect(
      arrayLongSlice([0, 1, false, 2, '', 3, 'soleil'], 3.6)
    ).toStrictEqual(['', 3, 'soleil']);
  });
  test('function to remove all values', () => {
    expect(arrayLongSlice(null)).toStrictEqual([]);
  });
  test('function to remove all values', () => {
    expect(
      arrayLongSlice([0, 1, false, 2, '', 3, 'soleil'], -2)
    ).toStrictEqual([0, 1, false, 2, '', 3, 'soleil']);
  });
});

describe('arrayLongSliceEnd', () => {
  test('function to remove last4 values', () => {
    expect(
      arrayLongSliceEnd([0, 1, false, 2, '', 3, 'soleil'], 3.6)
    ).toStrictEqual([0, 1, false]);
  });
  test('function to not remove any value', () => {
    expect(
      arrayLongSliceEnd([0, 1, false, 2, '', 3, 'soleil'], 0)
    ).toStrictEqual([0, 1, false, 2, '', 3, 'soleil']);
  });
  test('function to remove all values', () => {
    expect(arrayLongSliceEnd(null)).toStrictEqual([]);
  });
  test('function to remove 0 values', () => {
    expect(
      arrayLongSliceEnd([0, 1, false, 2, '', 3, 'soleil'], 8)
    ).toStrictEqual([]);
  });
});
