import { arrayBurst } from '../split';

describe('arrayBurst', () => {
  test('function to burst array in multiple array of 1', () => {
    expect(arrayBurst([0, 1, false, 2, '', 3, 'soleil'])).toStrictEqual([
      [0],
      [1],
      [false],
      [2],
      [''],
      [3],
      ['soleil'],
    ]);
  });

  test('function to burst array in multiple array of 2', () => {
    expect(arrayBurst([0, 1, false, 2, '', 3, 'soleil'], 2.2)).toStrictEqual([
      [0, 1],
      [false, 2],
      ['', 3],
      ['soleil'],
    ]);
  });

  test('function to burst array in multiple array of 3', () => {
    expect(arrayBurst([0, 1, false, 2, '', 3, 'soleil'], 2.8)).toStrictEqual([
      [0, 1, false],
      [2, '', 3],
      ['soleil'],
    ]);
  });

  test('function to burst array in multiple array of 0', () => {
    expect(arrayBurst([0, 1, false, 2, '', 3, 'soleil'], 0)).toStrictEqual([]);
  });

  test('function to burst empty array in multiple array', () => {
    expect(arrayBurst(null)).toStrictEqual([]);
  });
});
