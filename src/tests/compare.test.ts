import {
  arrayDifference,
  arrayIntersection,
  arraySymmetricDifference,
} from '../compare';

const array1 = [2, 5, 9, 1, 5, 8, 5];
const array2 = [3, 6, 0, 1, 5, 8, 5];

describe('arrayIntersection', () => {
  test('function to return common values', () => {
    expect(arrayIntersection(array1, array2)).toStrictEqual([5, 1, 5, 8, 5]);
  });
  test('function to return common values with no duplicates', () => {
    expect(arrayIntersection(array1, array2, true)).toStrictEqual([5, 1, 8]);
  });
});

describe('arrayDifference', () => {
  test('function to return common value', () => {
    expect(arrayDifference(array1, array2)).toStrictEqual([2, 9]);
  });
});

describe('arraySymmetricDifference', () => {
  test('function to return common value', () => {
    expect(arraySymmetricDifference(array1, array2)).toStrictEqual([
      2,
      9,
      3,
      6,
      0,
    ]);
  });
});
