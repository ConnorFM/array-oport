import { arrayToString } from '../edit';

describe('arrayBurst', () => {
  test('function to join without separator', () => {
    expect(arrayToString([0, 1, false, 2, '', 3, 'soleil'])).toStrictEqual(
      '0,1,false,2,,3,soleil'
    );
  });
  test('function to join with separator', () => {
    expect(arrayToString([0, 1, false, 2, '', 3, 'soleil'], '')).toStrictEqual(
      '01false23soleil'
    );
  });
});
