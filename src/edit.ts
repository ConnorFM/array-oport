export function arrayToString(array: unknown[], separator?: string): string {
  return array.join(separator);
}
