import { floatToInteger } from './split';

/**
 * Splices array of a specific value once
 */
export function removeOnce(array: unknown[], value: unknown): unknown[] {
  const copy = [...array];
  const index = copy.indexOf(value);
  if (index > -1) {
    copy.splice(index, 1);
  }
  return copy;
}

/**
 * Splices array of a specific value every time it is encountered
 */
export function removeAll(array: unknown[], value: unknown): unknown[] {
  return array.filter((item) => item !== value);
}

/**
 * Splices array of an array of value
 */
export function removeMultiValues(
  array: unknown[],
  values: unknown[]
): unknown[] {
  return array.filter((item) => !values.includes(item));
}

/**
 * Splices array of an array of value
 */
export function removeDuplicates(array: unknown[]): unknown[] {
  return [...new Set(array)];
}

/**
 * Remove value which equals 0, false, '' & null
 */
export function removeFalsy(array: unknown[]): unknown[] {
  const falsy = [0, false, '', null, undefined];
  return array.filter(
    (item: string | number | boolean) => !falsy.includes(item)
  );
}

/**
 * Removes multiple (number) values from the beginning of an array
 */
export function arrayLongSlice(array: unknown[], number = 1): unknown[] {
  const length = array == null ? 0 : array.length;
  return length
    ? array.slice(number < 0 ? 0 : floatToInteger(number), length)
    : [];
}

/**
 * Removes multiple (number) values from the beginning of an array
 */
export function arrayLongSliceEnd(array: unknown[], number = 1): unknown[] {
  const length = array == null ? 0 : array.length;
  number = length - floatToInteger(number);
  return length ? array.slice(0, number < 0 ? 0 : number) : [];
}
