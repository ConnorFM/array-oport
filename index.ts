'use strict';

import './src/compare';
import './src/find';
import './src/remove';
import './src/shuffle';
import './src/split';
